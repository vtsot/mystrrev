<?php

/**
 * @param string $string - переворачиваемая строка
 * @return string - перевернутая строка
 */
function myStrRev($string)
{
    // получить длину строки
    $strlen = strlen($string);
    
    // если строка из одного символа
    if (strlen($string) == 1) {
        return $string;
    }
    
    // не четная длина
    $isOdd = ($strlen %2 !== 0)
        ? true
        : false;
    
    // последний символ
    $lastSimbol = null;
    
    // если длина не четная
    if ($isOdd) {
        $lastSimbol = $string[$strlen-1];
        $string = substr($string, 0, $strlen-1);
        // получить длину строки
        $strlen = strlen($string);
    }
    
    // половина строки
    $halfLength = (strlen($string)/2)-1;
    
    // синдекс символа справа
    $indexRight = ($strlen-1);
    
    // обойти строку слева на право до половины строки
    for($indexLeft=0; $indexLeft <= $halfLength; $indexLeft++) {
        // символ слева 
        $symboLeft = $string[$indexLeft];
        // заменить слева 
        $string[$indexLeft] = $string[$indexRight];
        // заменить справа
        $string[$indexRight] = $symboLeft;
        // именьшить индекс справа
        --$indexRight;
    }
    
    // вернуть строку в обратном порядке
    // если есть последний символ для нечетной строки 
    // то конкатинируем его в начало строки
    return ($lastSimbol)
        ? $lastSimbol . $string
        : $string; 
}