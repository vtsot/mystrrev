<?php

/**
 * @param string $string - переворачиваемая строка
 * @return string - перевернутая строка
 */
function myStrRev2($string)
{   
    // половина строки
    $halfLength = (strlen($string)/2)-1;
    
    // синдекс символа справа
    $indexRight = (strlen($string)-1);
    
    // обойти строку слева на право до половины строки
    for($indexLeft=0; $indexLeft <= $halfLength; $indexLeft++) {
        // символ слева 
        $symboLeft = $string[$indexLeft];
        // заменить слева 
        $string[$indexLeft] = $string[$indexRight];
        // заменить справа
        $string[$indexRight] = $symboLeft;
        // именьшить индекс справа
        --$indexRight;
    }
    
    // вернуть строку в обратном порядке
    return $string; 
}